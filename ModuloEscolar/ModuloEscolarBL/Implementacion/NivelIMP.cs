﻿using ModuloEscolarDAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarBL.Implementacion{
    public class NivelIMP
    {
        private List<Nivel> oListNivel;
        public NivelIMP()
        {
            oListNivel = new List<Nivel>{
                new Nivel { IdNivel ="1",IdTurno="M", LetraNivel="I",Descripcion="Inicial"},
                new Nivel { IdNivel ="2",IdTurno="M", LetraNivel="P",Descripcion="Primaria"},
                new Nivel { IdNivel ="3",IdTurno="M", LetraNivel="S",Descripcion="Secundaria"},
                new Nivel { IdNivel ="4",IdTurno="T", LetraNivel="S",Descripcion="Secundaria"}
            };
        }
        public List<Nivel> ListarNivel ( string IdTurno){
            return oListNivel.FindAll( e => e.IdTurno == IdTurno);
        }
    }
}