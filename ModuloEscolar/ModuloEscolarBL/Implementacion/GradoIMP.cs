﻿using ModuloEscolarDAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarBL.Implementacion{
    public  class GradoIMP{
        private List<Grado> oListGrado;
        public GradoIMP(){
            oListGrado = new List<Grado> { 
                new Grado{IdGrado="1",IdNivel="1",NumeroGrado="3",Descripcion="Inicial 3 Años"},
                new Grado{IdGrado="2",IdNivel="1",NumeroGrado="4",Descripcion="Inicial 4 Años"},
                new Grado{IdGrado="3",IdNivel="1",NumeroGrado="5",Descripcion="Inicial 5 Años"},
                new Grado{IdGrado="4",IdNivel="2",NumeroGrado="1",Descripcion="Primaria I Grado"},
                new Grado{IdGrado="5",IdNivel="2",NumeroGrado="2",Descripcion="Primaria II Grado"},
                new Grado{IdGrado="6",IdNivel="2",NumeroGrado="3",Descripcion="Primaria III Grado"},
                new Grado{IdGrado="7",IdNivel="2",NumeroGrado="4",Descripcion="Primaria IV Grado"},
                new Grado{IdGrado="8",IdNivel="2",NumeroGrado="5",Descripcion="Primaria V Grado"},
                new Grado{IdGrado="9",IdNivel="2",NumeroGrado="6",Descripcion="Primaria VI Grado"},
                new Grado{IdGrado="10",IdNivel="3",NumeroGrado="1",Descripcion="Secundaria I Grado"},
                new Grado{IdGrado="11",IdNivel="3",NumeroGrado="2",Descripcion="Secundaria II Grado"},
                new Grado{IdGrado="12",IdNivel="3",NumeroGrado="3",Descripcion="Secundaria III Grado"},
                new Grado{IdGrado="13",IdNivel="3",NumeroGrado="4",Descripcion="Secundaria IV Grado"},
                new Grado{IdGrado="14",IdNivel="3",NumeroGrado="5",Descripcion="Secundaria V Grado"},
                new Grado{IdGrado="15",IdNivel="4",NumeroGrado="3",Descripcion="Secundaria III Grado"},
                new Grado{IdGrado="16",IdNivel="4",NumeroGrado="4",Descripcion="Secundaria IV Grado"},
                new Grado{IdGrado="17",IdNivel="4",NumeroGrado="5",Descripcion="Secundaria V Grado"},
            };
        }
        public List<Grado> ListarGrado(string IdNivel){
            return oListGrado.FindAll(e => e.IdNivel == IdNivel);
        }

    }
}