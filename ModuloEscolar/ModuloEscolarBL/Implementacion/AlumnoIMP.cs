﻿using ModuloEscolarDAL.Context;
using ModuloEscolarDAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarBL.Implementacion{
    public class AlumnoIMP{
        private List<Alumno> oListAlumno;
        private IEscolarRepositorio contexto;
        public AlumnoIMP(){

             contexto = new EFEscolarRepositorio();

            oListAlumno = new List<Alumno>() { 
                new Alumno { Id="201501", Nombre="Juan",ApellidoMaterno="Gonzales",ApellidoPaterno="Pardo",
                        FechaNacimiento= 20150821, CodigoModular="225012547265", Estado="A", 
                        TipoDocumentoIdentidad="01",NumeroDocumentoIdentidad="46257869", UbigeoNacimiento="150101"
                },
                new Alumno{ Id = "201502",Nombre = "Manuel",ApellidoMaterno = "Melandra",ApellidoPaterno = "Segundo",
                    FechaNacimiento =20150821,CodigoModular = "4652847861852",Estado = "A",
                    TipoDocumentoIdentidad = "02",NumeroDocumentoIdentidad = "564754171",UbigeoNacimiento = "150101"
                }
            };    
        }
        public List<Alumno> ListarAlumno() {
            return oListAlumno;
        }
        public Alumno ListarAlumno(string Id){
            return oListAlumno.SingleOrDefault(e => e.Id == Id); 
        }
        public List<Alumno> Buscar(string Busqueda)
        {
            return oListAlumno.Where(x => x.ApellidoPaterno.ToUpper().Contains(Busqueda.ToUpper()) ||
                                            x.ApellidoMaterno.ToUpper().Contains(Busqueda.ToUpper()) ||
                                            x.Nombre.ToUpper().Contains(Busqueda.ToUpper())).ToList();
        }
        public void GuardarAlumno(Alumno request){
            var cAlumno = request;
            //contexto.AlumnoRepository.Add(request);
            contexto.AlumnoRepository.Add(cAlumno);
            contexto.Commit();
        }
    }
}