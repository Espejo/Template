﻿using ModuloEscolarDAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarBL.Implementacion{
    public class EstadoIMP{
        private List<Estado> oListEstado;
        public EstadoIMP(){
            oListEstado = new List<Estado>(){
                new Estado {IdEstado ="A",Descripcion="Activo"},
                new Estado {IdEstado ="I",Descripcion="Inactivo"},
            };
        }
        public List<Estado> ListaEstado(){
            return oListEstado;
        }
    }
}