﻿using ModuloEscolarDAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarBL.Implementacion{
    public class MatriculaIMP
    {
        private List<Matricula> oListMatricula;
        public MatriculaIMP()
        {
            oListMatricula = new List<Matricula>{
                new Matricula {IdMatricula ="1", IdAlumno ="201501",Alumno ="Juan Hinostroza", FechaMatricula = new DateTime (2015,01,01), Estado ="Matriculado",
                    IdEstado ="M", IdSeccion ="16", Seccion ="Secundaria I Grado A", IdGrado ="10", IdNivel="3",IdTurno="M"},
                new Matricula {IdMatricula ="2", IdAlumno ="201502",Alumno ="Manuel Albarado", FechaMatricula = new DateTime (2015,01,01), Estado ="Matriculado",
                    IdEstado ="M", IdSeccion ="17", Seccion ="Secundaria II Grado A",IdGrado ="11", IdNivel="3",IdTurno="M"}
            };
        }
        public List<Matricula> ListarMatricula()
        {
            return oListMatricula;
        }
        public Matricula ListarMatricula(string IdMatricula)
        {
            return oListMatricula.SingleOrDefault(e => e.IdMatricula == IdMatricula);
        }
    }
}
