﻿using ModuloEscolarDAL.Context;
using ModuloEscolarDAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarBL.Implementacion{
    public class TurnoIMP{
        private List<Turno> oListTurbo;
        private IEscolarRepositorio contexto;
        public TurnoIMP(){
            contexto = new EFEscolarRepositorio();
            oListTurbo = new List<Turno>(){
                new Turno{Id="M",Descripcion ="Mañana"},
                new Turno{Id="T",Descripcion ="Tarde"}
            };
        }
        public List<Turno> ListarTurno(){
            return oListTurbo;
        }
        public void Guardar( Turno ETurno ) {
            contexto.TurnoRepository.Add(ETurno);
            contexto.Commit();
        }
    }
}