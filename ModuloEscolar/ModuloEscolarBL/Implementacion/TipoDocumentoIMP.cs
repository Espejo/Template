﻿using ModuloEscolarDAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarBL.Implementacion{
    public  class TipoDocumentoIMP{
        private List<TipoDocumentoIdentidad> oListTipoDocumento;
        public TipoDocumentoIMP(){
            oListTipoDocumento = new List<TipoDocumentoIdentidad>(){
                new TipoDocumentoIdentidad {IdTipoDoc ="01",Descripcion="DNI"},
                new TipoDocumentoIdentidad {IdTipoDoc ="02",Descripcion="Carnet de Extranjeria"},
                new TipoDocumentoIdentidad {IdTipoDoc ="03",Descripcion="Otros"}
            };
        }
        public List<TipoDocumentoIdentidad> ListaTipoDocumentoIdentidad(){
            return oListTipoDocumento;
        }
    }
}
