﻿using ModuloEscolarDAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarBL.Implementacion{
    public  class SeccionIMP{
        private List<Seccion> oListSeccion;
        public SeccionIMP(){
            oListSeccion = new List<Seccion>{
                new Seccion{IdGrado="1",IdSeccion="1",LetraSeccion="A",Descripcion="Inicial 3 Años A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="2",IdSeccion="2",LetraSeccion="A",Descripcion="Inicial 4 Años A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="3",IdSeccion="3",LetraSeccion="A",Descripcion="Inicial 5 Años A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="4",IdSeccion="4",LetraSeccion="A",Descripcion="Primaria I Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="4",IdSeccion="5",LetraSeccion="B",Descripcion="Primaria I Grado B",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="5",IdSeccion="6",LetraSeccion="A",Descripcion="Primaria II Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="5",IdSeccion="7",LetraSeccion="B",Descripcion="Primaria II Grado B",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="6",IdSeccion="8",LetraSeccion="A",Descripcion="Primaria III Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="6",IdSeccion="9",LetraSeccion="B",Descripcion="Primaria III Grado B",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="7",IdSeccion="10",LetraSeccion="A",Descripcion="Primaria IV Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="7",IdSeccion="11",LetraSeccion="B",Descripcion="Primaria IV Grado B",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="8",IdSeccion="12",LetraSeccion="A",Descripcion="Primaria V Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="8",IdSeccion="13",LetraSeccion="B",Descripcion="Primaria V Grado B",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="9",IdSeccion="14",LetraSeccion="A",Descripcion="Primaria VI Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="9",IdSeccion="15",LetraSeccion="B",Descripcion="Primaria VI Grado B",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="10",IdSeccion="16",LetraSeccion="A",Descripcion="Secundaria I Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="11",IdSeccion="17",LetraSeccion="A",Descripcion="Secundaria II Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="12",IdSeccion="18",LetraSeccion="A",Descripcion="Secundaria III Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="13",IdSeccion="19",LetraSeccion="A",Descripcion="Secundaria IV Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="14",IdSeccion="20",LetraSeccion="A",Descripcion="Secundaria V Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="15",IdSeccion="21",LetraSeccion="A",Descripcion="Secundaria III Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="16",IdSeccion="22",LetraSeccion="A",Descripcion="Secundaria IV Grado A",LimiteMatriculas=40,CantidadMatriculados=0},
                new Seccion{IdGrado="17",IdSeccion="23",LetraSeccion="A",Descripcion="Secundaria V Grado A",LimiteMatriculas=40,CantidadMatriculados=0}
            };
        }
        public List<Seccion> ListarSeccion(string IdGrado){
            return oListSeccion.FindAll(e => e.IdGrado == IdGrado);
        }
    }
}