﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarBL.Entidades{
    public  class Matricula{
        public string IdMatricula { get; set; }
        public DateTime FechaMatricula { get; set; }
        public string IdAlumno { get; set; }
        public string Alumno { get; set; }
        public string  IdSeccion { get; set; }
        public string Seccion { get; set; }
        public string IdEstado { get; set; }
        public string Estado { get; set; }

        public string IdGrado { get; set; }

        public string IdNivel { get; set; }

        public string IdTurno { get; set; }
    }
}