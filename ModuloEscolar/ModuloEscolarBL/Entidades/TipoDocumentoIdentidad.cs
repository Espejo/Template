﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarBL.Entidades{
    public class TipoDocumentoIdentidad{
        public string IdTipoDoc { get; set; }
        public string Descripcion { get; set; }
    }
}