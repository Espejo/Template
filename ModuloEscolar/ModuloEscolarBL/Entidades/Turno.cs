﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarBL.Entidades{
    public  class Turno{
        public string IdTurno { get; set; }
        public string Descripcion { get; set; }
    }
}