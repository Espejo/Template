﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarBL.Entidades{
    public class Grado{
        public string IdGrado { get; set; }
        public string IdNivel { get; set; }
        public string NumeroGrado { get; set; }
        public string Descripcion { get; set; }
    }
}
