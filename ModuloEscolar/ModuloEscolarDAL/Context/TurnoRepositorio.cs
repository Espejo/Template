﻿using ModuloEscolarDAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarDAL.Context{
    public class TurnoRepositorio : RepositorioGenerico.GenericRepository<EFEscolarRepositorio, Turno>
    {
        public TurnoRepositorio(EFEscolarRepositorio context) : base(context) { }
    }
}