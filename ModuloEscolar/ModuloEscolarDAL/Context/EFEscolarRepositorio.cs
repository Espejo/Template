﻿using ModuloEscolarDAL.Entidades;
using System.Data.Entity;
namespace ModuloEscolarDAL.Context{
    public class EFEscolarRepositorio : DbContext, IEscolarRepositorio{
        private readonly AlumnoRepositorio _AlumnoRepo;
        private readonly TurnoRepositorio _TurnoRepo;
        public IRepositorioGenerico<Alumno> AlumnoRepository { get { return _AlumnoRepo; } }
        public IRepositorioGenerico<Turno> TurnoRepository { get { return _TurnoRepo; } }
        public DbSet<Alumno> Alumno { get; set; }
        public DbSet<Turno> Turno { get; set; }
        public EFEscolarRepositorio(): base("name=ModuloEscolar"){
            _AlumnoRepo = new AlumnoRepositorio(this);
            _TurnoRepo = new TurnoRepositorio(this);
        }
        public void Commit(){this.SaveChanges();}
    }    
}