﻿using ModuloEscolarDAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarDAL.Context{
    public interface IEscolarRepositorio : IDisposable{
        IRepositorioGenerico<Alumno> AlumnoRepository { get; }
        //IRepositorioGenerico<Matricula> AsistenteRepository { get; }
        IRepositorioGenerico<Turno> TurnoRepository { get; }
        //IRepositorioGenerico<Nivel> NiveRepository { get; }
        //IRepositorioGenerico<Grado> GradoRepository { get; }
        //IRepositorioGenerico<Seccion> SeccionRepository { get; }
        //IRepositorioGenerico<TipoDocumentoIdentidad> TipoDpcumentoIdentidadRepository { get; }
        //IRepositorioGenerico<Estado> EstadoRepository { get; }
        void Commit();
    }
}