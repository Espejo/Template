﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using  ModuloEscolarDAL.Context;
using ModuloEscolarDAL.Entidades;
namespace ModuloEscolarDAL.Context{
    public class AlumnoRepositorio : RepositorioGenerico.GenericRepository<EFEscolarRepositorio,Alumno>{
        public AlumnoRepositorio(EFEscolarRepositorio context): base(context){}
    }
}
