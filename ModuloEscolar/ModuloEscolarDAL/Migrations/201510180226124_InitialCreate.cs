namespace ModuloEscolarDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {

            CreateTable(
                "dbo.Alumnoes",
                c => new
                    {
                        Id = c.Int (identity:true ),
                        Nombre = c.String(),
                        ApellidoPaterno = c.String(),
                        ApellidoMaterno = c.String(),
                        FechaNacimiento = c.Decimal (precision:8),
                        Estado = c.String(),
                        CodigoModular = c.String(),
                        UbigeoNacimiento = c.String(),
                        TipoDocumentoIdentidad = c.String(),
                        NumeroDocumentoIdentidad = c.String(),
                    })
            .PrimaryKey(t => t.Id);           
        }
        public override void Down(){
            DropTable("dbo.Alumnoes");
        }
    }
}
