namespace ModuloEscolarDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Turno : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Turnoes",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Turnoes");
        }
    }
}
