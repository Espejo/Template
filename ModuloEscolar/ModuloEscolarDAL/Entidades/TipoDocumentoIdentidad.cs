﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarDAL.Entidades
{
    public class TipoDocumentoIdentidad{
        public string IdTipoDoc { get; set; }
        public string Descripcion { get; set; }
    }
}