﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloEscolarDAL.Entidades
{
    public class Nivel
    {
        public string IdNivel { get; set; }
        public string IdTurno { get; set; }
        public string LetraNivel { get; set; }
        public string Descripcion { get; set; }
    }
}
