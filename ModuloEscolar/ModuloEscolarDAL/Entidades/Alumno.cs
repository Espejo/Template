﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarDAL.Entidades
{
    public class Alumno{
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public decimal FechaNacimiento { get; set; }
        public string Estado { get; set; }
        public string CodigoModular { get; set; }
        public string UbigeoNacimiento { get; set; }
        public string TipoDocumentoIdentidad { get; set; }
        public string NumeroDocumentoIdentidad { get; set; }
    }
}