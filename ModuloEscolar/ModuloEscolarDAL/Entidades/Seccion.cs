﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ModuloEscolarDAL.Entidades
{
    public  class Seccion{
        public string IdSeccion { get; set; }
        public string IdGrado { get; set; }
        public string LetraSeccion { get; set; }
        public string Descripcion { get; set; }
        public int LimiteMatriculas { get; set; }
        public int CantidadMatriculados { get; set; }
    }
}