﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModuloEscolarBL.Implementacion;
using ModuloEscolarDAL.Entidades;
using AutoMapper;
using ModuloEscolar.Models;
namespace ModuloEscolar.Controllers
{
    public class MatriculaController : Controller
    {
        // GET: Matricula
        private MatriculaIMP DTOMatricula;
        private TurnoIMP DTOTurno;
        private NivelIMP DTONivel;
        private GradoIMP DTOGrado;
        private SeccionIMP DTOSeccion;
        static MatriculaViewModel Model;
        public ActionResult Matricula()
        {
            DTOTurno = new TurnoIMP();
            DTONivel = new NivelIMP();
            DTOGrado = new GradoIMP();
            DTOSeccion = new SeccionIMP();
            Model = new MatriculaViewModel();
            Model.ListaTurno = new SelectList(DTOTurno.ListarTurno(), "IdTurno", "Descripcion", "");
            Model.ListaNivel = new SelectList(DTONivel.ListarNivel(""), "IdNivel", "Descripcion", "");
            Model.ListaGrado = new SelectList(DTOGrado.ListarGrado(""), "IdGrado", "Descripcion", "");
            Model.ListaSeccion = new SelectList(DTOSeccion.ListarSeccion(""), "IdSeccion", "Descripcion", "");
            return View(Model);
        }
        public ActionResult ListarMatricula() {
            DTOMatricula = new MatriculaIMP();
            List<Matricula> oLisMatricula = DTOMatricula.ListarMatricula ();
            return View(Mapper.Map<List<Matricula>, List<ListarMatriculaViewModel>>(oLisMatricula));
        }
        public ActionResult Modificar(string IdMatricula)
        {
            DTOMatricula = new MatriculaIMP();
            DTOTurno = new TurnoIMP();
            DTONivel = new NivelIMP();
            DTOGrado = new GradoIMP();
            DTOSeccion = new SeccionIMP();
            Matricula oMatricula = DTOMatricula.ListarMatricula(IdMatricula);
            Model = Mapper.Map<Matricula, MatriculaViewModel>(oMatricula);
            Model.ListaTurno = new SelectList(DTOTurno.ListarTurno(), "IdTurno", "Descripcion", oMatricula.IdTurno);
            Model.ListaNivel = new SelectList(DTONivel.ListarNivel(oMatricula.IdTurno), "IdNivel", "Descripcion", oMatricula.IdNivel);
            Model.ListaGrado = new SelectList(DTOGrado.ListarGrado(oMatricula.IdNivel), "IdGrado", "Descripcion", oMatricula.IdGrado);
            Model.ListaSeccion = new SelectList(DTOSeccion.ListarSeccion(oMatricula.IdGrado), "IdSeccion", "Descripcion", oMatricula.IdSeccion);
            return View("Matricula", Model);
        }
        [HttpPost]
        public ActionResult ListaNivel(  string IdTurno)
        {
            DTONivel = new NivelIMP();
            Model.ListaNivel = new SelectList(DTONivel.ListarNivel(IdTurno), "IdNivel", "Descripcion", "");
            return View("Matricula", Model);    
        }
        [HttpPost]
        public ActionResult ListaGrado(string IdNivel)
        {
            DTOGrado = new GradoIMP();
            Model.ListaGrado = new SelectList(DTOGrado.ListarGrado(IdNivel), "IdGrado", "Descripcion", "");
            return View("Matricula", Model);
        }
        [HttpPost]
        public ActionResult ListaSeccion(string IdGrado)
        {
            DTOSeccion = new SeccionIMP();
            Model.ListaSeccion = new SelectList(DTOSeccion.ListarSeccion(IdGrado), "IdSeccion", "Descripcion", "");
            return View("Matricula", Model);
        }

    }
}