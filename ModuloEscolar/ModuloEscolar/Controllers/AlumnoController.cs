﻿using AutoMapper;
using ModuloEscolar.Models;
using ModuloEscolarDAL.Entidades;
using ModuloEscolarBL.Implementacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
namespace ModuloEscolar.Controllers{
    public class AlumnoController : Controller{
        private AlumnoIMP DTOAlumno;
        private TipoDocumentoIMP DTOTipoDocumento;
        private EstadoIMP DTOEstado;
        // GET: Alumno
        public ActionResult Alumno(){
            DTOTipoDocumento = new TipoDocumentoIMP();
            DTOEstado = new EstadoIMP();
            AlumnoViewModel Model = new AlumnoViewModel() ;
            Model.ListaTipoDocumentoIdentidad = new SelectList(DTOTipoDocumento.ListaTipoDocumentoIdentidad(), "IdTipoDoc", "Descripcion", "");
            Model.ListaEstado = new SelectList(DTOEstado.ListaEstado(), "IdEstado", "Descripcion", "");
            return View(Model);
        }
        public ActionResult ListarAlumno(){
            DTOAlumno = new AlumnoIMP();
            List<Alumno> oListAlumno = DTOAlumno.ListarAlumno();
            return View(Mapper.Map<List<Alumno>, List<ListarAlumnoViewModel>>(oListAlumno));
        }
        public ActionResult Buscar(string Busqueda)
        {
            DTOAlumno = new AlumnoIMP();
            List<Alumno> oListAlumno = DTOAlumno.Buscar(Busqueda);
            return View("ListarAlumno", Mapper.Map<List<Alumno>, List<ListarAlumnoViewModel>>(oListAlumno));
        }
        public ActionResult Modificar(string Id){
            DTOAlumno = new AlumnoIMP();
            DTOTipoDocumento = new TipoDocumentoIMP();
            DTOEstado = new EstadoIMP();
            Alumno oAlumno = DTOAlumno.ListarAlumno(Id);
            AlumnoViewModel Model = Mapper.Map<Alumno, AlumnoViewModel>(oAlumno);
            Model.ListaTipoDocumentoIdentidad = new SelectList(DTOTipoDocumento.ListaTipoDocumentoIdentidad(), "IdTipoDoc", "Descripcion","01" );
            Model.ListaEstado = new SelectList(DTOEstado.ListaEstado(), "IdEstado", "Descripcion", "A");
            //Modelo con las listas de Tipo de documento
            return View("Alumno", Model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GuardarAlumno(AlumnoViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DTOAlumno = new AlumnoIMP();
                    DTOAlumno.GuardarAlumno(Mapper.Map<AlumnoViewModel, Alumno>(model));
                }
                return RedirectToAction("Ver", "Eventos", new { id = model.Id});
            }
            catch (DbEntityValidationException ex)
            {
                throw  ;
            }
        }
    }
}