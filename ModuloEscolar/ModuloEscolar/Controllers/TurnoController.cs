﻿using ModuloEscolar.Models;
using ModuloEscolarBL.Implementacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ModuloEscolarDAL.Entidades;
namespace ModuloEscolar.Controllers
{
    public class TurnoController : Controller
    {
        TurnoIMP DTOTurno;
        // GET: Turno
        public ActionResult Turno()
        {
            return View();
        }
        public ActionResult Guardar( TurnoViewModel model){
            try {
                if (ModelState.IsValid)
                {
                    DTOTurno = new TurnoIMP();
                    DTOTurno.Guardar(Mapper.Map<TurnoViewModel, Turno>(model));
                }
                return View();
            }
            catch (Exception ex)
            {
                
                return View();
            }
        }
    }
}