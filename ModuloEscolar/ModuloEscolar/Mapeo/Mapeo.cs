﻿using AutoMapper;
using ModuloEscolar.Controllers;
using ModuloEscolar.Models;
using ModuloEscolarDAL.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace ModuloEscolar.Mapeo{
    public class Mapeo{
        public static void ConfiguracionMapeo(){
            Mapper.CreateMap<Alumno, ListarAlumnoViewModel>();
            Mapper.CreateMap<Alumno, AlumnoViewModel>();
            Mapper.CreateMap<AlumnoViewModel, Alumno>();
            Mapper.CreateMap<Matricula, MatriculaViewModel>();
            Mapper.CreateMap<Matricula, ListarMatriculaViewModel>();
            //Mapper.CreateMap<Turno, TurnoViewModel>();
            Mapper.CreateMap<TurnoViewModel, Turno>();
        }
    }
}