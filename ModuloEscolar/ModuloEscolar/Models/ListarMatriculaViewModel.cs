﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace ModuloEscolar.Models{
    public class ListarMatriculaViewModel{
        [Display(Name = "Codigo")]
        public string IdMatricula { get; set; }
        [Display(Name = "Fecha Matricula")]
        public DateTime FechaMatricula { get; set; }
        public string Alumno { get; set; }
        public string Seccion { get; set; }        
        public string Estado { get; set; }
    }
}