﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace ModuloEscolar.Models{
    public class MatriculaViewModel{
        [Display(Name = "Codigo")]
        public string IdMatricula { get; set; }
        [Required]
        [Display(Name = "Fecha Matricula")]
        public DateTime FechaMatricula { get; set; }
        [Required]
        [Display(Name = "Codigo Alumno")]
        public string IdAlumno { get; set; }
        [Required]
        [Display(Name = "Turno")]
        public string IdTurno { get; set; }
        public SelectList ListaTurno { get; set; }
        [Required]
        [Display(Name = "Nivel")]
        public string IdNivel { get; set; }
        public SelectList ListaNivel { get; set; }
        [Required]
        [Display(Name = "Grado")]
        public string IdGrado { get; set; }
        public SelectList ListaGrado { get; set; }
        [Required]
        [Display(Name = "Seccion")]
        public string IdSeccion { get; set; }
        public SelectList ListaSeccion { get; set; }
        [Required]
        public string Estado { get; set; }
    }
}