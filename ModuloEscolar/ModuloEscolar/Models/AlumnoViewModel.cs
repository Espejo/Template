﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace ModuloEscolar.Controllers{
    public class AlumnoViewModel {        
        [Display(Name = "Codigo")]
        public string Id { get; set; }
        [Required]
        [Display(Name = "Nombre de Alumno")]
        public string Nombre { get; set; }
        [Required]
        [Display(Name = "Apellido Paterno")]
        public string ApellidoPaterno { get; set; }
        [Required]
        [Display(Name = "Apellido Materno")]
        public string ApellidoMaterno { get; set; }
        [Required]
        [Display(Name = "Fecha de Nacimiento")]
        public decimal FechaNacimiento { get; set; }
        [Required]
        public string Estado { get; set; }
        public SelectList ListaEstado { get; set; }
        [Required]
        [Display(Name = "Codigo Modular")]
        public string CodigoModular { get; set; }
        [Required]
        [Display(Name = "Ubigeo de Direccion")]
        public string UbigeoNacimiento { get; set; }
        [Required]
        [Display(Name = "Documento de Identidad")]
        public string TipoDocumentoIdentidad { get; set; }
        public SelectList ListaTipoDocumentoIdentidad { get; set; }
        [Required]
        [Display(Name = "Numero de Identidad ")]
        public string NumeroDocumentoIdentidad { get; set; }
    }
}